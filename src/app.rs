use log::{info, error, warn};

use crate::{renderer, renderpass, pipeline, texture, ssbo};


#[derive(Default, Debug)]  #[repr(C)]
struct ConfigData {
    exposure_contrast_saturation: [f32; 4]
}

pub struct App {
    window: winit::window::Window,
    renderer: renderer::Renderer,
    pipeline: pipeline::GPipeline,
    
    // data, NOTE: this is just for poc, in final version, this will be in a separate struct and 
    //             this bit of memory need to be managed well after discussion
    main_texture: texture::Texture,
    // main_texture: ssbo::SSBO,
    config_data: ConfigData,
    config_ssbo: ssbo::SSBO,
}

impl App {
    pub async fn new(event_loop: &winit::event_loop::EventLoop<()>, element_id: &str) -> Self {
        
        let window = winit::window::WindowBuilder::new()
        .with_title("POC - Jaysmito Mukherjee")
        .with_inner_size(winit::dpi::PhysicalSize::new(800, 800))
        .build(&event_loop)
        .unwrap();
        
        #[cfg(target_arch="wasm32")]
        App::setup_canvas(&window, element_id);
        
        let renderer = renderer::Renderer::new(&window).await;
        
        
        // let ssbo = ssbo::SSBO::new(4, renderer.device());
        let config_ssbo = ssbo::SSBO::new(std::mem::size_of::<ConfigData>(), renderer.device());
        let texture = texture::Texture::new(1, 1, renderer.device());

        let pipeline = pipeline::GPipeline::new(renderer.device(),
            &vec![
                (renderer.surface_config().format, wgpu::BlendState::REPLACE)
            ],
            include_str!("assets/shaders/shader.wgsl"),
            vec![&texture.bind_group_layout(), &config_ssbo.bind_group_layout()]);
        

        App {
            window              : window,
            renderer            : renderer,
            pipeline            : pipeline,
            main_texture        : texture,
            // main_texture        : ssbo,
            config_data         : ConfigData {
                exposure_contrast_saturation: [1.0, 1.0, 1.0, 1.0]
            },
            config_ssbo         : config_ssbo,
        }
    }
    
    pub fn on_event(&mut self, event: &winit::event::WindowEvent) {
        // info!("Event: {:?}", event);
    }

    pub fn on_update(&mut self) {

        self.renderer.begin_frame();
        match self.renderer.acquire_surface_texture() {
            Ok(_) => {},
            Err(wgpu::SurfaceError::Lost) => {
                self.renderer.reconfigure();
            },
            Err(wgpu::SurfaceError::OutOfMemory) => {
                panic!("Out of memory");
            },
            Err(e) => {
                error!("Error: {:?}", e);
            },
        }

        {
            let attachments = vec![renderpass::Attachment::color_from_view(self.renderer.surface_texture_view().unwrap())];
            let mut rpass = self.renderer.begin_render_pass(&attachments);

            // Draw here
            rpass.set_pipeline(self.pipeline.pipeline());
            rpass.set_bind_group(0, self.main_texture.bind_group(), &[]);
            rpass.set_bind_group(1, self.config_ssbo.bind_group(), &[]);
            rpass.draw(0..6, 0..1);
        }       
        self.renderer.end_render_pass();

        self.renderer.end_frame();
        self.renderer.present();


    }
    
    pub fn redraw(&self) {
        self.window.request_redraw();
    }
    
    pub fn upload_image(&mut self, id: &str, data: &[u8], width: u32, height: u32) {
        log::warn!("upload_image called with id: {}, width: {}, height: {}", id, width, height);
        log::warn!("data size: {} MB / {} MB", data.len() as f32 / 1024.0 / 1024.0, (width * height * 4) as f32 / 1024.0 / 1024.0);
        self.main_texture = texture::Texture::new(width, height, self.renderer.device());
        self.main_texture.upload(&self.renderer.queue(), data);

        // if self.main_texture.size() < data.len() {
        //     self.main_texture = ssbo::SSBO::new(data.len(), self.renderer.device());
        // }
        // self.main_texture.upload(&self.renderer.queue(), data);
    }

    pub fn set_exposure_constrast_saturation(&mut self, exposure: f32, contrast: f32, saturation: f32) {
        // warn!("set_exposure_constrast_saturation called with exposure: {}, contrast: {}, saturation: {}", exposure, contrast, saturation);
        self.config_data.exposure_contrast_saturation = [exposure, contrast, saturation, 1.0];
        self.config_ssbo.upload(&self.renderer.queue(), unsafe { std::slice::from_raw_parts(&self.config_data as *const _ as *const u8, std::mem::size_of::<ConfigData>()) });
    }

    #[cfg(target_arch="wasm32")]
    fn setup_canvas(window: &winit::window::Window, item_id: &str) {
        use winit::platform::web::WindowExtWebSys;
        
        let canvas = window.canvas();
        let document = web_sys::window().unwrap().document().unwrap();
        let element = document.get_element_by_id(item_id).unwrap();
        element.append_child(&canvas).unwrap();
        info!("Canvas appended to element: {}", item_id);
    }
}

