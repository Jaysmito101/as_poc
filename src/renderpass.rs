pub struct Attachment {
    view: wgpu::TextureView,
    clear_color: wgpu::Color,
    is_color: bool,
}

impl Attachment {
    pub fn color_from_view(view: wgpu::TextureView) -> Self {
        Self {
            view: view,
            clear_color: wgpu::Color {r: 0.2, g: 0.2, b: 0.2, a: 1.0},
            is_color: true,
        }
    }

    pub fn to_wgpu_color_attachment(&self) -> wgpu::RenderPassColorAttachment {
        wgpu::RenderPassColorAttachment {
            view: &self.view,
            resolve_target: None,
            ops: wgpu::Operations {
                load: wgpu::LoadOp::Clear(self.clear_color),
                store: wgpu::StoreOp::Store
            },
        }
    }

}

