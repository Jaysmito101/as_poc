struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) v_tex_coords: vec2<f32>,
};

@vertex
fn vs_main(
    @builtin(vertex_index) in_vertex_index: u32,
) -> VertexOutput {
    var out: VertexOutput;
    let c = (in_vertex_index >> 0u) & 1u;
    let b = (in_vertex_index >> 1u) & 1u;
    let a = (in_vertex_index >> 2u) & 1u;

    let x = f32(b | (a & c));
    let y = f32(a | (~b & c));

    out.clip_position = vec4<f32>( 2.0 * x - 1.0, 2.0 * y - 1.0, 0.0, 1.0);
    out.v_tex_coords = vec2<f32>(x, y);
    
    
    return out;
}

// Fragment shader

@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0) @binding(1)
var s_diffuse: sampler;

struct ConfigData {
    exposure_contrast_saturation: array<f32, 4>,
}

// a ssbo of u32s
@group(1) @binding(0)
var<storage, read> config: ConfigData;



@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    let image_size = textureDimensions(t_diffuse, 0);
    let image_width = f32(image_size.x);
    let image_height = f32(image_size.y);

    var color : vec4<f32>;
    var x = in.v_tex_coords.x;
    var y = in.v_tex_coords.y;

    if (image_width > image_height) {
        y -= (image_width - image_height) / (2.0 * image_width);
        y *= image_width / image_height;
    } else {
        x -= (image_height - image_width) / (2.0 * image_height);
        x *= image_height / image_width;
    }

    color = textureSample(t_diffuse, s_diffuse, vec2<f32>(x, y));

    // NOTE: this are applied here just for the POC
    //       in actual these should be applied in a 
    //       compute shader and alse the image will
    //       be stored in a storage buffer
 
    // exposure
    // color = vec4<f32>(1.0 - exp(-color.xyz * config.exposure_contrast_saturation[0] / 100.0), color.w);
    color = vec4<f32>(color.xyz * pow(2.0, config.exposure_contrast_saturation[0] / 100.0), color.w);


    // saturation
    let luminance = dot(color.xyz, vec3<f32>(0.2126, 0.7152, 0.0722));
    color = vec4<f32>(mix(vec3<f32>(luminance), color.xyz, config.exposure_contrast_saturation[2] / 100.0), color.w);

    // gamma correction
    color = vec4<f32>(pow(color.xyz, vec3<f32>(config.exposure_contrast_saturation[1] / 100.0f)), color.w);
    
    if (x < 0.0 || x > 1.0 || y < 0.0 || y > 1.0) {
        color = vec4<f32>(0.0, 0.0, 0.0, 1.0);
    }

    return color;
}
