use log::info;
use std::{cell::RefCell, borrow::Borrow};
use winit::event::{Event, WindowEvent};

#[cfg(target_arch="wasm32")]
use wasm_bindgen::prelude::*;


mod app;
mod renderpass;
mod renderer;
mod pipeline;
mod texture;
mod ssbo;

#[derive(Default)]
struct State {
    app: Option<app::App>
}

thread_local! {
    static STATE : RefCell<State> = RefCell::new(State::default());
}

#[cfg(target_arch="wasm32")]  #[wasm_bindgen]
pub async fn set_exposure_constrast_saturation(exposure: f32, contrast: f32, saturation: f32) {
    STATE.with(|s| {
        let state = &mut *s.borrow_mut();
        state.app.as_mut().unwrap().set_exposure_constrast_saturation(exposure, contrast, saturation);
    })
}

// This needs to be written in a better way !!!
#[cfg(target_arch="wasm32")]  #[wasm_bindgen]
pub async fn upload_image(id: &str, data: &[u8], width: u32, height: u32) {
    STATE.with(|s| {
        let state = &mut *s.borrow_mut();
        let app = state.app.as_mut().unwrap().upload_image(id, data, width, height);
    })
}

#[cfg(target_arch="wasm32")]  #[wasm_bindgen]
pub async fn setup_poc(element_id: &str) {

    cfg_if::cfg_if! {
        if #[cfg(target_arch="wasm32")] {
            // setup core utils
            std::panic::set_hook( Box::new(console_error_panic_hook::hook) );
            console_log::init_with_level(log::Level::Warn).expect("error initializing logger");
        } else {
            env_logger::init();
        }
    }
    
    let event_loop = winit::event_loop::EventLoop::new();
    
    let app = app::App::new(&event_loop, element_id).await;

    STATE.with(|s| {
        let state = &mut *s.borrow_mut();
        state.app = Some(app);
    });

    event_loop.run(move |event, _, control_flow| {
        match event {
            Event::WindowEvent {
                ref event ,
                ..
            } => match event {
                WindowEvent::CloseRequested => {
                    info!("Quitting...");
                    *control_flow = winit::event_loop::ControlFlow::Exit;
                },
                _ => {
                    STATE.with(|s| {
                        s.borrow_mut().app.as_mut().unwrap().on_event(event);
                    });
                },
            },
            Event::RedrawRequested(_) => {
                STATE.with(|s| {
                    s.borrow_mut().app.as_mut().unwrap().on_update();
                });
            },
            Event::MainEventsCleared => {
                STATE.with(|s| {
                    s.borrow().app.as_ref().unwrap().redraw();
                });
            },
            _ => {},
        }
    });
    
}
