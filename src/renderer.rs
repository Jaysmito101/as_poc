use crate::renderpass::Attachment;

pub struct Renderer {
    surface: wgpu::Surface,
    surface_config: wgpu::SurfaceConfiguration,
    device: wgpu::Device,
    queue: wgpu::Queue,
    instance: wgpu::Instance,
    encoder: Option<wgpu::CommandEncoder>,
    surface_texture: Option<wgpu::SurfaceTexture>,
}


impl Renderer {

    pub async fn new(window: &winit::window::Window) -> Self{

        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends: wgpu::Backends::BROWSER_WEBGPU,
            ..Default::default()
        });

        let surface = unsafe { instance.create_surface(window) }.unwrap();

        let adapter = instance.request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::default(),
            compatible_surface: Some(&surface),
            force_fallback_adapter: false,
        }).await.unwrap();

        log::warn!("Adapter: {:?}", adapter.get_info());

        let (device, queue) = adapter.request_device(
            &wgpu::DeviceDescriptor {
                features: wgpu::Features::empty(),
                limits: wgpu::Limits::downlevel_webgl2_defaults(),
                label: None,
            },
            None,
        ).await.unwrap();

        let surface_caps = surface.get_capabilities(&adapter);
        let surface_format = surface_caps.formats.iter()
            .copied()
            .filter(|f| f.is_srgb())
            .next()
            .unwrap_or(surface_caps.formats[0]);
        let present_mode = surface_caps.present_modes.iter()
            .copied()
            .filter(|&mode| mode == wgpu::PresentMode::Mailbox)
            .next()
            .unwrap_or(surface_caps.present_modes[0]);

        let surface_config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: window.inner_size().width,
            height: window.inner_size().height,
            present_mode: present_mode,
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
        };

        surface.configure(&device, &surface_config);

        Renderer {
            surface         : surface,
            surface_config  : surface_config,
            device          : device,
            queue           : queue,
            instance        : instance,
            encoder         : None,
            surface_texture : None,
        }
    }

    pub fn resize(&mut self, window: &winit::window::Window) {
        self.surface_config.width = window.inner_size().width;
        self.surface_config.height = window.inner_size().height;
        self.reconfigure();
    }

    pub fn surface_texture_view(&self) -> Option<wgpu::TextureView> {
        let view = self.surface_texture.as_ref()?.texture.create_view(&wgpu::TextureViewDescriptor{
            ..Default::default()
        });
        Some(view)
    }

    pub fn encoder(&mut self) -> Option<&mut wgpu::CommandEncoder> {
        self.encoder.as_mut()
    }

    pub fn device(&self) -> &wgpu::Device {
        &self.device
    }

    pub fn queue(&self) -> &wgpu::Queue {
        &self.queue
    }

    pub fn surface_config(&self) -> &wgpu::SurfaceConfiguration {
        &self.surface_config
    }

    pub fn reconfigure(&self) {
        self.surface.configure(&self.device, &self.surface_config);
    }

    pub fn begin_frame(&mut self) {
        if self.encoder.is_some() {
            panic!("begin_frame called twice without end_frame");
        }

        self.encoder = Some(self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("RendererEncoder"),
        }));
    }

    pub fn end_frame(&mut self) {
        let encoder = self.encoder.take().unwrap();
        self.queue.submit(std::iter::once(encoder.finish()));
    }

    pub fn begin_render_pass<'a>(&'a mut self, color_attachments: &'a Vec<Attachment>) -> wgpu::RenderPass<'a> {
        let attachments_wgpu = color_attachments.iter().map(|a| Some(a.to_wgpu_color_attachment())).collect::<Vec<_>>();
        let descriptor = Some(wgpu::RenderPassDescriptor {
            label: Some("RenderPass"),
            color_attachments: &attachments_wgpu,
            depth_stencil_attachment: None,
            timestamp_writes: None,
            occlusion_query_set: None,
        });

        self.encoder.as_mut().unwrap().begin_render_pass(descriptor.as_ref().unwrap())
    }

    pub fn end_render_pass(&mut self) {
    }


    // swap chain stuff
    pub fn acquire_surface_texture(&mut self) -> Result<(), wgpu::SurfaceError> {
        self.surface_texture = Some(self.surface.get_current_texture()?);

        Ok(())
    }

    pub fn present(&mut self) {
        let surface_texture = self.surface_texture.take().unwrap();
        surface_texture.present()
    }

    



}

