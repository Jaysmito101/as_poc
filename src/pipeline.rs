pub struct GPipeline {
    pipeline_layout: wgpu::PipelineLayout,
    pipeline: wgpu::RenderPipeline,
}

impl GPipeline {

    pub fn new(device: &wgpu::Device, color_targets: &Vec<(wgpu::TextureFormat, wgpu::BlendState)>, shader_source: &str, bind_grp_layouts: Vec<&wgpu::BindGroupLayout>) -> Self {

        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("ShaderModule"),
            source: wgpu::ShaderSource::Wgsl(shader_source.into())
        });

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("PipelineLayout"),
            bind_group_layouts: bind_grp_layouts.as_slice(),
            push_constant_ranges: &[],
        });

        let color_targets = color_targets.iter()
                .copied()
                .map(|f_b| -> Option<wgpu::ColorTargetState> {
                    Some(wgpu::ColorTargetState {
                        format: f_b.0,
                        blend: Some(f_b.1),
                        write_mask: wgpu::ColorWrites::ALL,
                    })
                })
                .collect::<Vec<Option<wgpu::ColorTargetState>>>();

        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("GPipeline"),
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: color_targets.as_slice()
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Cw,
                cull_mode: Some(wgpu::Face::Back),
                polygon_mode: wgpu::PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask : !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        Self {
            pipeline_layout,
            pipeline
        }
    }

    pub fn pipeline(&self) -> &wgpu::RenderPipeline {
        &self.pipeline
    }

}